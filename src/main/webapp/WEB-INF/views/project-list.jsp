<%@ page import="java.util.List" %>
<%@ page import="ru.lab4.model.Project" %>
<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<html>
<head>
    <title>Projects</title>
</head>
<body>
<table border="2">
    <tr>
        <td>ID</td>
        <td>NAME</td>
        <td>begin</td>
        <td>end</td>
        <td>created</td>
        <sec:authorize access="hasRole('ADMINISTRATOR')">
                <td></td>
                <td></td>
        </sec:authorize>
    </tr>
    <% for (Project p : (List<Project>) request.getAttribute("projects")) { %>
    <tr>
        <td><%=p.getId()%></td>
        <td><%=p.getName()%></td>
        <td><%=p.getDateBegin()%></td>
        <td><%=p.getDateEnd()%></td>
        <td><%=p.getCreated()%></td>
        <sec:authorize access="hasRole('ADMINISTRATOR')">
                <td>
                    <form action="delete/<%=p.getId()%>" method="post">
                        <button type="submit">DELETE</button>
                    </form>
                </td>
                <td>
                    <form action="/project/edit/<%=p.getId()%>" method="get">
                        <button type="submit">EDIT</button>
                    </form>
                </td>
        </sec:authorize>
    </tr>
    <% } %>
</table>

<sec:authorize access="hasRole('ADMINISTRATOR')">
    <form action="/project/edit" method="post">
        <button type="submit">CREATE</button>
    </form>
</sec:authorize>

<sec:authorize access="isAuthenticated()">
    <form action="/logout" method="post">
        <button type="submit">LOGOUT</button>
    </form>
</sec:authorize>
</body>
</html>
