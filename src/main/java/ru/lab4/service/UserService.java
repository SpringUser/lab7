package ru.lab4.service;

import edu.emory.mathcs.backport.java.util.Collections;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.lab4.model.Role;
import ru.lab4.model.RoleType;
import ru.lab4.model.User;
import ru.lab4.repository.UserRepository;

import javax.annotation.PostConstruct;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @PostConstruct
    private void init() {
        initUser("admin", "admin", RoleType.ADMINISTRATOR);
        initUser("test", "test", RoleType.USER);
    }

    private void initUser(final String login, final String pass, final RoleType roleType) {
        final User user = userRepository.findByLogin(login);
        if (user != null) return;
        createUser(login, pass, roleType);
    }

    private void createUser(String login, String pass, RoleType roleType) {

        final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(passwordEncoder.encode(pass));

        final Role role = new Role();
        role.setUser(user);
        role.setRoleType(roleType);

        user.setRoles(Collections.singletonList(role));

        userRepository.save(user);
    }
}
