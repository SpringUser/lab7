package ru.lab4.model;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Table(name = "app_Role")
public class Role {

    @Id
    private String id = UUID.randomUUID().toString();

    @ManyToOne
    private User user;

    @Column
    @Enumerated(EnumType.STRING)
    private RoleType roleType = RoleType.USER;

    public Role() {
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public RoleType getRoleType() {
        return roleType;
    }

    public void setRoleType(RoleType roleType) {
        this.roleType = roleType;
    }

    @Override
    public String toString() {
        return roleType.name();
    }
}
