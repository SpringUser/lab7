package ru.lab4.model;

public enum RoleType {
    ADMINISTRATOR,
    USER;
}
