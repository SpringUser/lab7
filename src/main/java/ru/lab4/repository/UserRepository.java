package ru.lab4.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import ru.lab4.model.User;

public interface UserRepository extends PagingAndSortingRepository<User, String> {

    User findByLogin(String login);

}
